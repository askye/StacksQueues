#include "main.h"

// Amethyst Skye // CSE222 // 02-08-2021 //
// Programming Assignment 3 //
// A program written in C which allows the user to interact with a stack
// and queue. 
// Legal commands are as follows:
//
// s - print the current contents of the stack on one line, separated by 
// single spaces. The stack will read from left to right in order from top of 
// stack to bottom. This will also set the current mode to "stack mode".
//
// q - print the current contents of the queue on one line, separared by 
// single spaces. The queue will read from left to right with the head of 
// the queue on the left. This will also set the current mode to "queue
// mode".
//
// # - type any integer to push onto current stack, or insert at the tail of
// current queue (depending on which mode is active).
//
// p - (stack mode) pop the top of stack and print item popped.
// p - (queue mode) remove current head from queue and print that item.
//
// Q - exit the program.
//
// anything else - a list of legal commands will be displayed for user.

/////////////////////////////////////////////////////////////////////////////

int main()
{
struct node *stack=initStack(); //sentinel for stack
struct queue *queue=initQueue(); //sentinel for queue

char str[120]; //user input 
int input=0; //command interactions
int state=0; //0 for stack mode, 1 for queue mode

//welcome message//
printf("Welcome. This program allows you to interact with a stack and queue.\n");
displayHelp();
//main program//
while(1){ //infinite loop
  printf(">"); //user prompt
  fgets(str,120,stdin);
  int ret=sscanf(str,"%d",&input); //recieve input command save return value
//begin in stack mode//
  switch(str[0]){ 
//quit program//
    case 'Q':
      free(stack); //free memory
      free(queue); 
      exit(0);
      break;
    case '?':
      displayHelp(); //help message
      break;
//stack mode//
    case 's':
      state=0; //change state
      printf("Switching to stack mode.\n");
      displayStack(stack); //print stack  
      break;
//queue mode//
    case 'q': 
      state=1; //change state
      printf("Switching to queue mode.\n");
      displayQueue(queue); //print queue
      if(ret == 1){
      }
      break;
//pop(stack) or remove(queue)//
    case 'p':
      if(state == 0){ //stack pop
        if(stackEmpty(stack) == 1){
          printf("Stack is empty.\n"); //empty stack case
        }
        else{
          int lastNum=stack->data; //save last push
          stack=pop(stack); //implement pop
          printf("Removed %d\n",lastNum);
        }
      }
      else if(state == 1){ //remove head of queue
        if(queueEmpty(queue) == 1){
          printf("Queue is empty.\n"); //empty queue case        
        }
        else{
        int headQ=queue->head->data; //save head data
        removeH(queue); //remove from queue
        printf("Removed %d\n",headQ);
        }
      }
      break;
//user types # or invalid input
    default: 
    if(state == 0){ //stack state
      if(ret == 1){ //for legal int
        stack=push(stack,input); //push input onto TOS
      }
      else{ //for invalid input
        displayHelp();
      }
    }
    if(state == 1){ //queue state
      if(ret ==1){ //for legal int
        add(queue, input);
      }
      else{ //for invalid input
        displayHelp(); //help message
      }
    } 
    break;
  } //end of switch statement
} //end of while != Q loop
free(stack);
free(queue);
} //end of function
