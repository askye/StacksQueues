pa3: main.o initStack.o initQueue.o push.o displayStack.o pop.o stackEmpty.o add.o displayQueue.o remove.o queueEmpty.o displayHelp.o
	gcc -g -o pa3 main.o initStack.o initQueue.o push.o displayStack.o pop.o stackEmpty.o add.o displayQueue.o remove.o queueEmpty.o displayHelp.o
main.o: main.c main.h
	gcc -c main.c
initStack.o: initStack.c
	gcc -c initStack.c
initQueue.o: initQueue.c
	gcc -c initQueue.c
push.o: push.c
	gcc -c push.c
displayStack.o: displayStack.c
	gcc -c displayStack.c
pop.o: pop.c
	gcc -c pop.c
stackEmpty.o: stackEmpty.c
	gcc -c stackEmpty.c
add.o: add.c
	gcc -c add.c
displayQueue.o: displayQueue.c
	gcc -c displayQueue.c
remove.o: remove.c
	gcc -c remove.c
queueEmpty.o: queueEmpty.c
	gcc -c queueEmpty.c
displayHelp.o: displayHelp.c
	gcc -c displayHelp.c

clean:
	rm pa3 main.o initStack.o initQueue.o push.o displayStack.o pop.o stackEmpty.o add.o displayQueue.o remove.o queueEmpty.o displayHelp.o
