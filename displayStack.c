#include "main.h"

//This function will accept a struct node (stack) and print its contents.
//The order in which the contents are displayed will be from top of
//stack to bottom of stack (left to tright respectively).

void displayStack(struct node *stack){

struct node *curr=stack; //look at what sentinel of stack is pointing to
if(stackEmpty(stack)==1){ //stack is empty
  printf("Stack is empty.\n");
}
else{
printf("TOS->"); //indicating top of stack
  while(curr->next != NULL){//iterate through data in stack
    printf("%d ",curr->data); //print data with single space
    curr=curr->next; //shift to next node
  } //end of iterating
printf("\n");
} //end of "if" statement
} //end of function
