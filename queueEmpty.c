#include "main.h"

//queueEmpty.c//
//This function will accept a struct node (queue) and determine whether
//or not the queue is empty. The function will return a 1 if empty, and a 0
//otherwise.

int queueEmpty(struct queue *queue)
{
  if(queue->head == NULL){ //check if queue is equal to NULL
    return(1); //empty
  }
  else{
    return(0); //not empty
  }
} //end of function
