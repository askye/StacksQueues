#include "main.h"

//displayQueue.c//

//This function will accept a struct node (queue). The purpose of the function
//is to display the current contents of the running queue.

void displayQueue(struct queue *queue)
{
struct node *head=queue->head; //head node
  if(head == NULL){ //empty queue
    printf("Queue is empty.\n");
  }
  else{ 
    printf("HEAD-->"); //point to head of queue
    while(head != NULL){ //iterate through data in queue
      printf("%d ",head->data); //print data with single space
      head=head->next; //shift to next node
    } //end of iterations
  printf("<--TAIL\n"); //tail of queue
  } //end of "if" statement
} //end of function
