#include "main.h"

//pop.c//
//This function will remove the node that is at the top of the current stack.
//The function accepts a struct node (stack) and interger(num) and will return
//the struct node with TOS removed.

struct node *pop(struct node *stack)
{
struct node *temp=stack; //current TOS
stack=stack->next; //shift stack to the right
free(temp); //free TOS
return(stack); //return updated stack
}
