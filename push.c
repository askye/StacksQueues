#include "main.h"

//This function will be passed a struct node (stack), along with an integer
//value (num) which will be added to the stack.
//The function will return a value indicating success or failure of insertion
//(1 or 0 respectively).

struct node *push(struct node*stack, int num)
{
struct node *new, *sent; 
new=malloc(sizeof(struct node)); //allocate memory for new node
sent=stack; //TOS
  if(new == NULL){ //if stack is full
    free(new);
    exit(0); //fail
  }
    new->data=num; //insert data
    new->next=sent; //switch TOS with new node
    sent=new; 
    return(sent);
} //end of function
