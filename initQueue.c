#include "main.h"

//initQueue.c//
//This function initializes an empty queue and returns a node with memory
//allocated.

struct queue *initQueue()
{
struct queue *queue=malloc(sizeof(struct queue)); //allocate space for queue
queue->head=queue->tail=NULL; //initialize sentinel
return(queue); //send empty queue
}

