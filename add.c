#include "main.h"

//add.c//
//This function will accept a struct node (queue) and a integer (num). The 
//purpose of this function is to add the passed num to the tail of the
//queue. The function will return the new queue.



void add(struct queue *queue, int num)
{
struct node *new; //new node and sentinel node
new=malloc(sizeof(struct node)); //allocate memory for new node
  if(new == NULL){
    free(new);
    exit(0); //out of memory
  }
  else{
    new->data=num; //insert data
  }
  if(queue->tail == NULL){ //for empty queue
    queue->head=queue->tail=new;//insert new node
    new->next=NULL; //next to NULL 
  }
  else{ //add new node at end of queue
    queue->tail->next=new;
    queue->tail=new;
    new->next=NULL;
  }
} //end of function    
