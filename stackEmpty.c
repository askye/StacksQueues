#include "main.h"

//stackEmpty.c//
//This function will accept a struct node (stack) and determine whether
//or not the stack is empty. The function will return a 1 if empty, and a 0 
//otherwise.

int stackEmpty(struct node *stack)
{
  if(stack->next == NULL){ //check if stack is equal to NULL
    return (1); //empty
  }
  else{
    return(0); //not empty
  }
}
