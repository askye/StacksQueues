#include "main.h"

//initStack.c//
//This function initializes an empty stack and returns node with memory
//allocated.

struct node *initStack()
{
struct node *stack=malloc(sizeof(struct node)); //allocate memory for stack
stack->next=NULL; //initialize sentinel
return(stack); //send empty stack
}

