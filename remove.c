#include "main.h"

//remove.c//
//This function will accept a struct node (queue), remove the data from the
//head of the queue, and return the struct node (queue) back.

void removeH(struct queue *queue)
{
struct node *temp=queue->head; //current head of queue
queue->head=queue->head->next; //shift queue to right
free (temp); //release head node
} //end of function
