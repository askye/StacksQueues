//main.h//

//This file is included in each of the functions within this program.
//The contents will provide all necessary libraries and call any of the
//functions within the program.


//Libraries//
#include <stdio.h>
#include <stdlib.h>

//struct node definition//
struct node{
	int data;
	struct node *next;
};

struct queue{
	struct node *head;
	struct node *tail;
};

void displayHelp(); //displays command options for user

//Stack functions//
struct node *initStack(); //initialize stack
struct node *push(struct node *, int); //push onto stack
struct node *pop(struct node *); //pop off of stack
void displayStack(struct node *); //display current stack
int stackEmpty(struct node *); //check if stack empty

//Queue functions//
struct queue *initQueue(); //initialize queue
void add(struct queue *, int); //add to queue
void displayQueue(struct queue *); //display current queue
void removeH(struct queue *); //remove head of queue
int queueEmpty(struct queue *); //check if queue empty
