#include <stdio.h>

//displayHelp.c//

//This function will display a list of all possible command options for
//interacting with the main program.

void displayHelp()
{
printf("Please use one of the following commands:\n");
printf("# - push number onto stack or add number to queue\n");
printf("p - pop stack or remove tail from queue\n");
printf("s - display contents of stack\n");
printf("q - display contents of queue\n");
printf("Q - quit\n");
printf("? - display help message\n");
}
